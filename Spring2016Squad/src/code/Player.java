package code;

import code.tile.Tile;

import java.util.ArrayList;

import code.Board;
import code.Pawn;

/**
 * This is the player class. 
 * Each of the player requires a boolean, two ints and three strings, one board and one pawn as parameters.
 * The Driver will initialize the players with correct configuration.
 * Initially player1 will start with pawn at (2,2); player2 will start with pawn at(2,4); player3 will start with pawn at(4,2); player4 will start with pawn at(4,4);
 * The player also has getter methods to retrieve the values of the parameters since they will change as the game goes on.
 * @author Hanming Liu 
 */
public class Player {
	private boolean _turn;
	private int _magicwands;
	private String _ingredient1;
	private String _ingredient2;
	private String _ingredient3;
	private Board _board;
	private Pawn _pawn;
	private ArrayList<Integer> _ingrediants;
	
	public Player(boolean turn, Board board, Pawn pawn){
		_ingrediants = new ArrayList<Integer>();
		_turn = turn;
		_magicwands = 3;
		ArrayList<Integer> list = Driver.IngrediantList();
		_ingredient1 = "" + Driver.randIngr(list);
		_ingredient2 = "" + Driver.randIngr(list);
		_ingredient3 = "" + Driver.randIngr(list);
		_board = board;
		_pawn = pawn;
	}
	/**
	 * Returns the turn info of the player, if returns true then currently it is this player's turn, false otherwise.
	 * @author Hanming Liu
	 * 	 
	 */
	
	public boolean getTurn(){
		return _turn;
	}
	
	/**
	 * Returns player's current score.
	 * @author Hanming Liu
	 */
	public int getScore(){
		int score = 0;
		for(Integer i : _ingrediants){
			score = score + i.intValue();
		}
		return score;
	}
	
	/**
	 * Returns player's chances of magicwands left.
	 * @author HanmingLiu
	 */
	public int getMagicwands(){
		return _magicwands;
	}
	/**
	 * Returns Player's first ingredient.
	 * @author Hanming Liu
	 */
	
	public String getIngredient1(){
		return _ingredient1;
	}
	
	/**
	 * Returns Player's second ingredient.
	 * @author Hanming Liu
	 */
	
	public String getIngredient2(){
		return _ingredient2;
	}
	
	/**
	 * Returns Player's third ingredient.
	 * @author Hanming Liu
	 */
	
	public String getIngredient3(){
		return _ingredient3;
	}
	
	public void addIngrediant(int i){
		_ingrediants.add(i);
	}

	public void movePawn(Tile endpoint){
		if(Driver.NUMBEROFPLAYERS==2){
			if(	_board.availableTileMovements(this._pawn.get_boardTile()).contains(endpoint)){
				this._pawn.set_boardTile(endpoint);
				this._turn= false;
				if(this==Driver._player1){Driver._player2._turn=true;}
				if(this==Driver._player2){Driver._player1._turn=true;}
		}
			if(Driver.NUMBEROFPLAYERS==4){
				if(	_board.availableTileMovements(this._pawn.get_boardTile()).contains(endpoint)){

					this._pawn.set_boardTile(endpoint);
					this._turn= false;
					if(this == Driver._player1){Driver._player2._turn= true;}
					if(this == Driver._player2){Driver._player3._turn= true;}
					if(this == Driver._player3){Driver._player4._turn= true;}
					if(this == Driver._player4){Driver._player1._turn= true;}
				}	
			}
		}
	}
	/**returns the tile that the player's pawn is on
	 * 
	 * @return Tile
	 * @author Matthew
	 */
	public Tile getPawnLocation(){
		return _pawn.get_boardTile();
	}
}
