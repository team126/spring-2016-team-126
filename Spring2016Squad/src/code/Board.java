package code;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import code.tile.Tile;
/**
 * NOTE: TOP LEFT TILE IS (0,0)
 * This is the Board in which the game would be played on.
 * A board consists of a nested ArrayList of BoardTiles( the most basic tiles in the game).
 * A board's dimension is 7X7.
 * A board will be randomized when it is initialized.
 * A board also has a freeTile which is not on the board but would be used by any player once at a time to push the tiles.
 * A board can call a push method to push a column or a row; and a getter method to get any BoardTile on the board. 
 * @author HanmingLiu
 *
 */
public class Board {
	public final static int BOARDSIZE = 7;
	private ArrayList<ArrayList<Tile>> _board;
	private Tile _freeTile;
	private HashSet<observable.Updateable> _obs;

	public Board(){
		_board = new ArrayList<ArrayList<Tile>>();
		_obs = new HashSet<observable.Updateable>();
		ArrayList<Integer> ingres = Driver.IngrediantList(); 
		for(int x=0; x<BOARDSIZE; x++){
			ArrayList<Tile> newColunm = new ArrayList<Tile>();
			
			for(int y = 0; y<BOARDSIZE;y++){
				
				//random int: ThreadLocalRandom.current().nextInt(1,4)
				Tile addThis = new Tile();
				if ( x>0 && x<6 && y>0 && y<6 && !((x==2 || x==4) && (y==2 || y==4)) ){
					addThis.takeIngrediant(Driver.randIngr(ingres));
				}
				newColunm.add(addThis);

			}
			_board.add(newColunm);

		}
		_freeTile = new code.tile.Tile();
	}

	
	/**
	 * This is a getter method which the board can call to get any specific BoardTile on the board. 
	 * This method will accept two int values to locate the BoardTile.
	 * This method will return a BoardTile.
	 * @author HanmingLiu
	 */
	public Tile getBoardTile(int x, int y){
		return _board.get(x).get(y);
	}
	/**
	 * pathNorth returns a boolean value true iff there is a valid path north of the tile at the x,y coordinate in the parameters.
	 * 
	 * @param x x index for tile
	 * @param y y index for tile
	 * @return boolean if there's a path to the north tile
	 * @author MatthewGreene,Sangwoo
	 */
	public boolean pathNorth(int x,int y){
		if(y-1 < 0 ){
			return false;
		}
		if(getBoardTile(x,y).get_north() && getBoardTile(x,y-1).get_south()){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * pathEast returns a boolean value true iff there is a valid path east of the tile at the x,y coordinate in the parameters.
	 * 
	 * @param x x index for tile
	 * @param y y index for tile
	 * @return boolean if there's a path to the east tile
	 * @author MatthewGreene,Sangwoo
	 */
	public boolean pathEast(int x,int y){
		if(x+1 >= BOARDSIZE ){
			return false;
		}
		if(getBoardTile(x,y).get_east() && getBoardTile(x+1,y).get_west()){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * pathSouth returns a boolean value true iff there is a valid path south of the tile at the x,y coordinate in the parameters.
	 * 
	 * @param x x index for tile
	 * @param y y index for tile
	 * @return boolean if there's a path to the south tile
	 * @author MatthewGreene,Sangwoo
	 */
	public boolean pathSouth(int x,int y){
		if(y+1 >= BOARDSIZE ){
			return false;
		}
		if(getBoardTile(x,y).get_south() && getBoardTile(x,y+1).get_north()){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * pathWest returns a boolean value true iff there is a valid path west of the tile at the x,y coordinate in the parameters.
	 * 
	 * @param x x index for tile
	 * @param y y index for tile
	 * @return boolean if there's a path to the western tile
	 * @author MatthewGreene,Sangwoo
	 */
	public boolean pathWest(int x,int y){
		if(x-1 < 0 ){
			return false;
		}
		if(getBoardTile(x,y).get_west() && getBoardTile(x-1,y).get_east()){
			return true;
		}
		else{
			return false;
		}
	}
	/**availableTileMovements returns a HashSet of legal positions a pawn can move to from a certain input tile.
	 * 
	 * @param t starting position that a pawn would be on.
	 * @return HashSet<Tile> of all valid end tile positions.
	 * @authors Nandi, Matthew, Sangwoo
	 */
	public HashSet<Tile> availableTileMovements(Tile t){
		HashSet<Tile> solution = new HashSet<Tile>();
		HashSet<Tile> tobeadded = new HashSet<Tile>();

		solution.add(t);
		tobeadded.add(t);
		while(!(solution.size()==tobeadded.size()-1)){

			solution.addAll(tobeadded);
			for(Tile a:solution){
				Point p = this.LocationofBoardTile(a);
				if(this.pathNorth(p.x, p.y)){
					tobeadded.add(this.getBoardTile(p.x, p.y-1));
				}
				if(this.pathEast(p.x, p.y)){
					tobeadded.add(this.getBoardTile(p.x+1, p.y));
				}
				if(this.pathSouth(p.x, p.y)){
					tobeadded.add(this.getBoardTile(p.x, p.y+1));
				}
				if(this.pathWest(p.x, p.y)){
					tobeadded.add(this.getBoardTile(p.x-1, p.y));
				}
			}
			
		}
		return solution;
	}
	
	/**accessor for freetile.
	 * 
	 * @return freetile.
	 */
	public Tile get_FreeTile(){
		return this._freeTile;
	}
	
	/**Alternate push method with char input determining 2 booleans. does same thing.
	 * @param direction char first letter of direction of push
	 * @param index int int value of the pushed row/col.
	 * @return boolean true iff board modified.
	 * @author Matthew
	 */
	public boolean push(char direction, int index){
		switch(direction){
		case ('n'):
			return pushNorth(index);
		case ('e'):
			return pushEast(index);
		case ('s'):
			return pushSouth(index);
		case ('w'):
			return pushWest(index);
		}
		return false;
	}
	
	/**
	 * This method will accept a specific tile as a parameter and returns a point.
	 * This point indicates the position of the tile which the pawn is on. 
	 * @author Hanming Liu
	 */

	public Point LocationofBoardTile(Tile t){
		for(int x =0; x < BOARDSIZE ;x ++){
			for(int y=0; y< BOARDSIZE ; y++){
				if(t == this.getBoardTile(x, y)){
					return new Point(x,y);
				}
			}
		}
		return null;
	}
	
	/**Push method pushes the _freeTile tile into the _board 
	 * at odd numbered indexes that move all the tiles in a 
	 * row or column in a certain direction. this will move 
	 * 1 tile off the board, and that tile becomes the new 
	 * _freeTile, as the old _freeTile is now on the board.
	 * 
	 * @param rowCol boolean true means row, false means column.
	 * @param positive boolean designates direction. true means south or east, false means north or west.
	 * @param positionNumber int row or colunm index number.
	 * @return boolean true if tiles moved false if no tiles were moved.
	 * @author MatthewGreene
	 */
	public boolean push(boolean rowCol, boolean positive, int positiveNumber){
		if(rowCol){
			if(positive){
				return pushEast(positiveNumber);
			}else{
				return pushWest(positiveNumber);
			}
		}else{
			if(positive){
				return pushSouth(positiveNumber);
			}else{
				return pushNorth(positiveNumber);
			}
		}
	}

	private boolean pushNorth(int colIndex){
		if(colIndex%2 == 1){
			Tile temp;
			temp = getBoardTile(colIndex,0);
			for (int y = 0; y < BOARDSIZE-1; y++){

				_board.get(colIndex).set(y, getBoardTile(colIndex,y+1));
			}
			_board.get(colIndex).set(BOARDSIZE-1, _freeTile);
			_freeTile = temp;
			return true;
		}else{
			return false;
		}
	}


	private boolean pushSouth(int colIndex){
		if(colIndex%2 == 1){
			Tile temp;
			temp = getBoardTile(colIndex,BOARDSIZE-1);
			for (int y = BOARDSIZE - 1; y > 0; y--){

				_board.get(colIndex).set(y, getBoardTile(colIndex,y-1));
			}
			_board.get(colIndex).set(0, _freeTile);
			_freeTile = temp;
			return true;
		}else{
			return false;
		}
	}


	private boolean pushEast(int rowIndex){
		if(rowIndex%2 == 1){
			Tile temp;
			temp = getBoardTile(BOARDSIZE-1,rowIndex);
			for (int x = BOARDSIZE - 1; x > 0; x--){

				_board.get(x).set(rowIndex, getBoardTile(x-1,rowIndex));
			}
			_board.get(0).set(rowIndex, _freeTile);
			_freeTile = temp;
			return true;
		}else{
			return false;
		}
	}


	private boolean pushWest(int rowIndex){
		if(rowIndex%2 == 1){
			Tile temp;
			temp = getBoardTile(0,rowIndex);
			for (int x = 0; x < BOARDSIZE - 1; x++){

				_board.get(x).set(rowIndex, getBoardTile(x+1,rowIndex));
			}
			_board.get(BOARDSIZE-1).set(rowIndex, _freeTile);
			_freeTile = temp;
			return true;
		}else{
			return false;
		}
	}
	public void callObservers(){
		for(observable.Updateable i : _obs){
			i.update();
		}
	}
	/**adds Updateable class to board. wil update when callObservers() is called.
	 * 
	 * @param o Updateable
	 */
	public void addObs(observable.Updateable o){
		this._obs.add(o);
	}


}

