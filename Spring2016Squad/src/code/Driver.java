package code;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Driver{
	/**
	 * The Driver will initialize the players and the board when the game starts.
	 * Players are initialized as static.
	 * @author Hanming Liu
	 *
	 */
	public static Player _player1;
	public static Player _player2;
	public static Player _player3;
	public static Player _player4;
	public static int NUMBEROFPLAYERS;
	private static int TURN;
	/**
	 * 
	 * @Unfinished players need to inherit names.
	 */
	public static void main(String[] args) {
		NUMBEROFPLAYERS = args.length;
		TURN = 1;
		code.Board board = new code.Board();
		for(int c = 0;c<args.length;c++){
			switch(c){
			case 0:
				_player1 = new Player(true ,board, new code.Pawn(board,board.getBoardTile(2, 2)));
				break;
			case 1:
				_player2 = new Player(false,board, new code.Pawn(board,board.getBoardTile(2, 4)));
				break;

			case 2:
				_player3 = new Player(false,board, new code.Pawn(board,board.getBoardTile(4, 2)));
				break;
			case 3:
				_player4 = new Player(false,board, new code.Pawn(board,board.getBoardTile(4, 4)));
				break;
			default:
				break;
			}
		}

		gui.Gui gamewindow = new gui.Gui();
		gamewindow.startGame(board);
	}
	public static void advanceTurn(){
		TURN++;
		if(TURN>=NUMBEROFPLAYERS){
			TURN = 1;
		}
	}
	public static int playerTurnNumber(){
		return TURN;
	}
	public static ArrayList<Integer> IngrediantList(){
		ArrayList<Integer> output = new ArrayList<Integer>();
		for(int i = 0;i<21;i++){
			int n = i;
			if( n == 0 ){
				n = 25;
			}
			output.add(n);
		}
		return output;
	}
	public static int randIngr(ArrayList<Integer> list){
		return list.remove(ThreadLocalRandom.current().nextInt(0,list.size()));
	}
}
