package code.tile;

import java.util.concurrent.ThreadLocalRandom;

/**Also referred to as BoardTile
 * this class is the basic tile on boards.
 * 
 */
public class Tile {

	/**
	 * Token class objects to add in tile class
	 */
	protected AToken _ingeidient;
	/**
	 * Boolean that defines if the tile has a northern opening
	 */

	protected boolean _north;

	/**
	 * Boolean that defines if the tile has a western opening
	 */
	protected boolean _west;
	/**
	 * Boolean that defines if the tile has a southern opening
	 */
	protected boolean _south;
	/**
	 * Boolean that defines if the tile has a eastern opening
	 */
	protected boolean _east;

	/**
	 * Basic constructor. initializes random variables 
	 * for the directional inputs.
	 * 
	 */
	public Tile(){
		_ingeidient = new emptyToken(this);
		setRandBridges();
		while(!ensureBridgeNumberRight()){	}
	}

	private void setRandBridges(){
		if(ThreadLocalRandom.current().nextInt(0,2)==1){
			_north = true;
		}else{
			_north = false;
		}
		if(ThreadLocalRandom.current().nextInt(0,2)==1){
			_south = true;
		}else{
			_south = false;
		}
		if(ThreadLocalRandom.current().nextInt(0,2)==1){
			_east = true;
		}else{
			_east = false;
		}
		if(ThreadLocalRandom.current().nextInt(0,2)==1){
			_west = true;
		}else{
			_west = false;
		}
	}

	private boolean ensureBridgeNumberRight(){
		int number = 0;
		if (this.get_north()){
			number++;
		}
		if (this.get_east()){
			number++;
		}
		if (this.get_south()){
			number++;
		}
		if (this.get_west()){
			number++;
		}
		if (number >= 2 && number <=3) {
			return true;
		}else{
			int locationSwap = ThreadLocalRandom.current().nextInt(0,4);
			switch(locationSwap){
			case 0:
				_north = !_north;
				break;
			case 1:
				_south = !_south;
				break;
			case 2:
				_east = !_east;
				break;
			case 3:
				_west = !_west;
				break;
			}
		}
		return false;
	}
	/**
	 * This constructor will create a tile that is identical 
	 * to a tile parameter "copy". However, it will have no 
	 * Ingredient on it.
	 * 
	 * @param copy tile that will be cloned.
	 * @author Aniruddha & Matthew
	 */
	public Tile(Tile copy) {
		_ingeidient = new emptyToken(this);

		_east = copy.get_east();
		_north = copy.get_north();
		_south = copy.get_south();
		_west = copy.get_west();

	}
	/**
	 * This constructor creates a hardcoded tile with the openings directly equal to params.
	 * 
	 * @param north sets the _north opening
	 * @param east sets the _east opening
	 * @param south sets the _south opening
	 * @param west sets the _west opening
	 * @author Matthew
	 */
	public Tile(boolean north,boolean east,boolean south,boolean west){
		_ingeidient = new emptyToken(this);
		_north = north;
		_east = east;
		_south = south;
		_west = west;
	}
	
	/**
	 * accesser for _north
	 * 
	 * @return returns the _north boolean
	 */
	public boolean get_north(){
		return _north;
	}
	/**
	 * accesser for _east
	 * 
	 * @return returns the _east boolean
	 */
	public boolean get_east(){
		return _east;
	}
	/**
	 * accesser for _south
	 * 
	 * @return returns the _south boolean
	 */
	public boolean get_south(){
		return _south;
	}
	/**
	 * accesser for _west
	 * 
	 * @return returns the _west boolean
	 */
	public boolean get_west(){
		return _west;
	}

	/**
	 * set the directions that the tile 
	 * opens 90 degrees counter clockwise. 
	 * north will become west, west becomes 
	 * south, etc.
	 * Only to be called when the tile is not 
	 * on a board, otherwise known as a free tile.
	 * 
	 */
	public void rotateCounterClockwise(){
		boolean temp;
		temp = _north;
		_north = _east;
		_east = _south;
		_south = _west;
		_west = temp;
	}
	/**
	 * rotates the tile directions clockwise.
	 * 
	 */
	public void rotateClockwise(){
		boolean temp;
		temp = _north;
		_north = _west;
		_west = _south;
		_south = _east;
		_east = temp;
	}
	/**
	 * equals method. I had no idea what I was doing here, but it returns true if the 4 openings are equal.
	 * 
	 * @param tile
	 * @return if equal.
	 * @author MatthewG 
	 */
	public boolean equals(Tile tile){
		return (this._east==tile.get_east() && this._west==tile.get_west() && this._north==tile.get_north() && this._south==tile.get_south());

	}
	/**this method gives the ingredient it has to the 
	 * passed pawn class. The tile will no longer have 
	 * an any ingredient after this operation.
	 * 
	 * @param p
	 * @author MatthewG
	 */
	public int giveIngrediant(){
		return _ingeidient.giveIngredient();
	}
	
	/**If the Tile has no ingeidient then it 
	 * will accept the int. if it does have 
	 * an ingrediant already, it will throw an exeption.
	 * 
	 * 
	 * @param i
	 */
	public void takeIngrediant(int i) {
		this._ingeidient.newIngredient(i);
	}
	
	protected void changeToken(AToken t){
		this._ingeidient = t;
	}
	
	/**look at the token value.
	 * 
	 * @return String of the number of ingredient.
	 */
	public String peek() {
		int i = this._ingeidient.peek();
		if(i==-1){
			return "";
		}
		return ""+i;
	}
	
	/**Extreme data structure work from here on out.
	 * 
	 * @author MatthewG
	 *
	 */
	private static abstract class AToken{
		protected Tile _owner;
		public abstract boolean hasDatum();
		public abstract Integer peek();
		public abstract int giveIngredient();
		public abstract void newIngredient(int i);
	}
	
	private static class emptyToken extends AToken{
		
		public emptyToken(Tile t){
			_owner = t;
		}
		
		@Override
		public boolean hasDatum() {
			return false;
		}

		@Override
		public int giveIngredient() {
			throw new IllegalStateException("emptyToken has no data.");
		}

		@Override
		public void newIngredient(int i) {
			_owner.changeToken(new nonEmptyToken(_owner, i));
			
		}

		@Override
		public Integer peek() {
			return -1;
		}
		
	}
	
	private static class nonEmptyToken extends AToken{
		
		private int _Value;
		
		public nonEmptyToken(Tile t, int value){
			_owner = t;
			_Value = value;
		}
		
		@Override
		public boolean hasDatum() {
			return true;
		}

		@Override
		public int giveIngredient() {
			_owner.changeToken(new emptyToken(_owner));
			return _Value;
		}

		@Override
		public void newIngredient(int i) {
			throw new IllegalStateException("Tile cannot accept another Token, it already has one.");
		}

		@Override
		public Integer peek() {
			return this._Value;
		}
		
	}

	
}
