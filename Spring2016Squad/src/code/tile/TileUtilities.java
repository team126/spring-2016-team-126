package code.tile;

public class TileUtilities {
	//Tile Type
	public static final char T = 'T';
	public static final char L = 'L';
	public static final char I = 'I';
	//TileSpin
	public static final int N = 0;
	public static final int E = 90;
	public static final int S = 180;
	public static final int W = 270;
	
	/**This method takes a tile and determines the type of tile it is.
	 * 
	 * 
	 * @param t insert Tile
	 * @return T,L, or I
	 * @throws IllegalArgumentException If the tile does not conform to a type.
	 */
	public static char tileType(Tile t){
		int truth = 0;
		if(t.get_north()){
			truth++;
		}
		if(t.get_east()){
			truth++;
		}
		if(t.get_south()){
			truth++;
		}
		if(t.get_west()){
			truth++;
		}
		if(truth == 3){
			return T;
		}
		if(truth == 2){
			if(t.get_north()==t.get_south()){
				return I;
			}else{
				return L;
			}
		}
		throw new IllegalArgumentException("Tile is an illegal type");
	}
	
	/**given a tile, it will calculate the 
	 * 
	 * @param t Tile parameter
	 * @param tileType return from code.tile.TileUtilities.TileRotation(t)
	 * @return N,E,S, or W
	 * @throws IllegalArgumentException for some stupid ass reason, if this thing throws, then the fucking tile is so dumb. it should never be the cause of throws, instead the tileType should just throw through this method.
	 */
	public static int TileRotation(Tile t,char tileType){
		switch(tileType){
		case T:
			if(!t.get_north()){
				return N;
			}
			if(!t.get_east()){
				return E;
			}
			if(!t.get_south()){
				return S;
			}
			if(!t.get_west()){
				return W;
			}
		case L:
			if(t.get_north()){
				if(t.get_east()){
					return N;
				}else{
					return W;
				}
			}else{
				if(t.get_east()){
					return E;
				}else{
					return S;
				}
			}
		case I:
			if(t.get_north()){
				return N;
			}else{
				return E;
			}
		default:
			throw new IllegalArgumentException("tileRotaion has failed, something is incredibly wrong.");
		}
	}
}
