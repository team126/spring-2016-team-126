package code;

import java.awt.Point;

import code.tile.Tile;

/**
 * 
 * 
 * 
 * This class is a pawn on the board.
 * 
 * User Story:
 * As a pawn I will always be on a tile.
 * As a pawn I can move to accessible tiles.
 * A pawn is moved when the according tile is pushed.
 * A pawn, if pushed off the board, will be placed on the tile at 
 * 	the opposite side of the board in the same row/column. 
 * 
 * @author Sangwoo
 *
 */

public class Pawn {

	/**
	 * This variable is the Board that the pawn belongs to.
	 *  
	 */
	private Board _board;
	private Tile _location;
	/**
	 * Constructor for Pawn class
	 * @param bard: this parameter is a pre-existing board that the pawn belongs to.
	 */
	public Pawn(Board bard, Tile toil){

		_board = bard;
		_location = toil;

	}

	/**
	 * Move the pawn to adjacent opened BoardTile.
	 */

	public Tile get_boardTile() {
		return _location;
	}



	public void set_boardTile(Tile _boardTile) {
		this._location = _boardTile;
	}

	/**@see code.Board.LocationofBoardTile
	 */

	public Point LocationofBoardTile(Tile t){
		return _board.LocationofBoardTile(t);
	}
	
	/**moves the pawn to the arg tile, and picks up the ingredient from the tile.
	 * 
	 * @param t
	 * @return gives the int value of the ingredient it has picked up after movement.
	 *
	 * @throws IllegalStateExeption will throw this if tile has no ingredient on it.
	 * 
	 */
	public int move(Tile t){
		_location = t;
		return this.pickUpIngridiantToPlayer();
	}
	
	/**to be called after Pawn moves.
	 * 
	 * 
	 */
	private int pickUpIngridiantToPlayer(){
		// TODO Auto-generated method stub
		return _location.giveIngrediant();
	
	}
}