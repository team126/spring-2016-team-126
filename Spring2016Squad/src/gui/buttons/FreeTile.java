package gui.buttons;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import code.Board;
import code.tile.Tile;
import code.tile.TileUtilities;
import imported.RotatedIcon;
import observable.Updateable;

public class FreeTile extends ClickableTile implements Updateable{
	
	public FreeTile(Board bard){
		super(9, 9, bard);
		update();
	}
	
	@Override
	public Tile retrieveTile(){
		return _b.get_FreeTile();
	}
	
}
