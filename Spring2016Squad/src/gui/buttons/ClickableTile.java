/**
 * 
 */
package gui.buttons;

import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import code.Board;
import code.Driver;
import code.tile.Tile;
import code.tile.TileUtilities;
import imported.RotatedIcon;
import observable.Updateable;

/**This Jbutton extension adds specific functionality to tie in with the system. It must be constructed with coordinates it will read from.
 * 
 * @author Matthew
 *
 */
public class ClickableTile extends JButton implements Updateable {

	public static final String T_IMAGE = "src/images/ESW Tile.jpg";
	public static final String L_IMAGE = "src/images/NE Tile.jpg";
	public static final String I_IMAGE = "src/images/NS Tile.jpg";
	public static final ImageIcon P1 = new ImageIcon("src/images/R Player.jpg");
	public static final ImageIcon P4 = new ImageIcon("src/images/Y Player.jpg");
	public static final ImageIcon P3 = new ImageIcon("src/images/P Player.jpg");
	public static final ImageIcon P2 = new ImageIcon("src/images/B Player.jpg");
	public static final int IMGSIZE = 80;
	
	private Point _location;
	protected Board _b;

	public ClickableTile(int xlocal,int ylocal,Board b){
		construct(xlocal,ylocal,b);	
	}
	
	
	private void construct(int x, int y, Board b){
		_location = new Point(x,y);
		_b = b;
		_b.addObs(this);
		this.setMargin(new Insets(0, 0, 0, 0));
        this.setHorizontalTextPosition(JButton.CENTER);
        this.setVerticalTextPosition(JButton.CENTER);
		update();
	}
	
	/**Image addition and conversion method
	 * 
	 * @throws IOException
	 */
	public ImageIcon addImgs(Icon tile, Icon pawn) throws IOException{
		BufferedImage n = new BufferedImage(IMGSIZE,IMGSIZE,BufferedImage.TYPE_INT_ARGB);
		tile.paintIcon(null,n.getGraphics(),0,0);
		BufferedImage m = new BufferedImage(IMGSIZE,IMGSIZE,BufferedImage.TYPE_INT_ARGB);
		pawn.paintIcon(null,m.getGraphics(),0,0);
		
		BufferedImage combined = new BufferedImage(IMGSIZE, IMGSIZE, BufferedImage.TYPE_INT_ARGB);
		// paint both images, preserving the alpha channels
		Graphics g = combined.getGraphics();
		g.drawImage(n, 0, 0, null);
		g.drawImage(m, 0, 0, null);
		
//		ImageIO.write(combined, "JPG", new File("twoInOne.jpg"));
		
		return new ImageIcon(combined);
	}
	
	public Tile retrieveTile(){
		return _b.getBoardTile(_location.x, _location.y);
	}
	
	/**make the button run this method to reflect the current state of the model.
	 * 
	 * 
	 */
	public void update(){
		Tile t = retrieveTile();
		char tileType = TileUtilities.tileType(t);
		int rotation = TileUtilities.TileRotation(t, tileType);
		Icon ii = null;
		switch(tileType){
		case TileUtilities.T:
			ii = new ImageIcon(T_IMAGE);
			break;
		case TileUtilities.I:
			ii = new ImageIcon(I_IMAGE);
			break;
		case TileUtilities.L:
			ii = new ImageIcon(L_IMAGE);
			break;
		}
		//  add rotation to the ImageIcon.
		RotatedIcon n;
		ii = new RotatedIcon(ii,rotation);
		
		//  add pawns to ImageIcon
		for(int i = 0;i<Driver.NUMBEROFPLAYERS;i++){
			switch(i){
			case 0:
				if(Driver._player1.getPawnLocation()==t){
					try {
						ii = addImgs(ii, P1);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case 1:
				if(Driver._player2.getPawnLocation()==t){
					try {
						ii = addImgs(ii, P2);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case 2:
				if(Driver._player3.getPawnLocation()==t){
					try {
						ii = addImgs(ii, P3);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case 3:
				if(Driver._player4.getPawnLocation()==t){
					try {
						ii = addImgs(ii, P4);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			}
		}
		this.setSize(IMGSIZE, IMGSIZE);
		this.setIcon(ii);
		this.setText(t.peek());
		

		
	}
	
	
}
