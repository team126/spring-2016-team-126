package gui.buttons;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import code.Board;

public class PushButtonListener extends JButton implements ActionListener{
	private static int SIZE = 80;
	private Board _board;
	private char DIRECTION;
	private int INDEX;
	
	public PushButtonListener(Board b,char pushDirection, int pushIndex){
		_board = b;
		INDEX = pushIndex;
		DIRECTION = pushDirection;
		this.addActionListener(this);
        this.setHorizontalTextPosition(JButton.CENTER);
        this.setVerticalTextPosition(JButton.CENTER);
		this.setPreferredSize(new Dimension(SIZE,SIZE));
		this.setBackground(Color.BLACK);
		switch(pushDirection){
		case 'n':
			//this.setText("^"+pushWhat());
			this.setIcon(new ImageIcon("src/images/up.jpg"));
			break;
		case 'e':
			//this.setText("==>"+pushWhat());
			this.setIcon(new ImageIcon("src/images/right.jpg"));
			break;
		case 's':
			//this.setText("s"+pushWhat());
			this.setIcon(new ImageIcon("src/images/down.jpg"));
			break;
		case 'w':
			//this.setText("<=="+pushWhat());
			this.setIcon(new ImageIcon("src/images/left.jpg"));
			break;
		default: throw new IllegalArgumentException("Only 'n','s','e','w' are allowed in PushButtonListener. illegal character.");
		}
	}
	private String pushWhat(){
		return INDEX + "" + DIRECTION;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		_board.push(DIRECTION, INDEX);
		_board.callObservers();
	}

}
