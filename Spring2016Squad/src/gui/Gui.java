package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui extends JFrame{
	private JPanel _panel;
	public Gui(){ 
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		_panel = new JPanel();
		
	}
	
	public void startGame(code.Board board){
		this.add(_panel);
		_panel.add(new InteractiveBoard(board));
		_panel.add(new InfoPanel(board));
		this.pack();
		
	}

}
