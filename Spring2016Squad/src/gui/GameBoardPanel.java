package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import code.Board;
import gui.buttons.ClickableTile;

public class GameBoardPanel extends JPanel{
	
	public GameBoardPanel(Board bard){
		this.setLayout(new GridLayout(7,7,0,0));
		this.setVisible(true);
		for(int y=0; y<7; y++){
			for(int x=0;x<7;x++){
				ClickableTile c = new ClickableTile(x,y,bard);
				this.add(c);
			}
		}
	}

}
