package gui.bottompanels;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import code.Board;
import gui.buttons.PushButtonListener;

public class PanelTop extends JPanel {
	
	public PanelTop(Board b){
		this.setLayout(new FlowLayout(FlowLayout.CENTER,80+12,0));
		this.add(new PushButtonListener(b, 's', 1));
		this.add(new PushButtonListener(b, 's', 3));
		this.add(new PushButtonListener(b, 's', 5));
		this.setBackground(Color.BLACK);
	}

}
