package gui.bottompanels;

import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import gui.buttons.FreeTile;

import code.Board;
import code.tile.Tile;
import code.tile.TileUtilities;
import gui.buttons.ClickableTile;

public class FreeTileRotater extends JPanel {
	
	private Board b;
	
	public FreeTileRotater(Board bard){
	this.setLayout(new GridLayout(2,2));
	this.setVisible(true);
	b = bard;
		
	JPanel panel = new JPanel();
	FreeTile c = new FreeTile(b);
	panel.add(c);
	
	/*
	
	final String T_IMAGE = "src/images/ESW Tile.jpg";
	final String L_IMAGE = "src/images/NE Tile.jpg";
	final String I_IMAGE = "src/images/NS Tile.jpg";
	
	Tile t = b.get_FreeTile();
	
	
	char tileType = TileUtilities.tileType(t);
			Icon ii = null;
	
			switch(tileType){
			case TileUtilities.T:
				ii = new ImageIcon(T_IMAGE);
				break;
			case TileUtilities.I:
				ii = new ImageIcon(I_IMAGE);
				break;
			case TileUtilities.L:
				ii = new ImageIcon(L_IMAGE);
				break;
			}
		
		c.setIcon(ii);
		
		*/
	
	JButton buton = new JButton("<<<");
	buton.addActionListener(new FreetileCClockwiseListener(panel,c));
	JButton buton1 = new JButton(">>>");
	buton1.addActionListener(new FreetileClockwiseListener(panel,c));
	
	
	panel.add(buton);
	panel.add(buton1);
	
	panel.setVisible(true);
	this.add(panel);
	
	}
	
	
}
