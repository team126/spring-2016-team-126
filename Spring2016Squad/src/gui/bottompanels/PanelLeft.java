package gui.bottompanels;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

import code.Board;
import gui.buttons.PushButtonListener;

public class PanelLeft extends JPanel {
	
	public PanelLeft(Board b){
		this.setLayout(new GridLayout(0,1,0,12));
		JPanel git = new JPanel();
		git.setBackground(Color.BLACK);
		JPanel fed = new JPanel();
		fed.setBackground(Color.BLACK);
		JPanel morty = new JPanel();
		morty.setBackground(Color.BLACK);
		JPanel rick = new JPanel();
		rick.setBackground(Color.BLACK);
		this.add(git);
		this.add(new PushButtonListener(b, 'e', 1));
		this.add(fed);
		this.add(new PushButtonListener(b, 'e', 3));
		this.add(morty);
		this.add(new PushButtonListener(b, 'e', 5));
		this.add(rick);
		this.setBackground(Color.BLACK);
	}

}
