package gui.bottompanels;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import code.Board;
import gui.buttons.PushButtonListener;

public class PanelBottom extends JPanel {
	
	public PanelBottom(Board b){
		this.setLayout(new FlowLayout(FlowLayout.CENTER,80+12,0));
		this.add(new PushButtonListener(b, 'n', 1));
		this.add(new PushButtonListener(b, 'n', 3));
		this.add(new PushButtonListener(b, 'n', 5));
		this.setBackground(Color.BLACK);
	}
	
}
