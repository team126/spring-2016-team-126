package gui.bottompanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.imageio.ImageIO;

import code.Board;
import gui.buttons.FreeTile;
import code.tile.Tile;
import code.tile.TileUtilities;

public class FreetileClockwiseListener extends FreeTileRotater implements ActionListener {
	
	static JPanel fe;
	static Board b;
	
	private FreeTile c1;
	
	
	public FreetileClockwiseListener(JPanel panel, FreeTile c2) {
	
		super(b);
		fe=panel;
		c1=c2;
	}
	
	public void actionPerformed (ActionEvent e) {
		final String T_IMAGE = "src/images/ESW Tile.jpg";
		final String L_IMAGE = "src/images/NE Tile.jpg";
		final String I_IMAGE = "src/images/NS Tile.jpg";
		
		Tile t = b.get_FreeTile();
		t.rotateClockwise();
		
		char tileType = TileUtilities.tileType(t);
				Icon ii = null;
		
				switch(tileType){
				case TileUtilities.T:
					ii = new ImageIcon(T_IMAGE);
					break;
				case TileUtilities.I:
					ii = new ImageIcon(I_IMAGE);
					break;
				case TileUtilities.L:
					ii = new ImageIcon(L_IMAGE);
					break;
				}
			
			c1.setIcon(ii);
		
	}

}
