package gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JPanel;

import gui.bottompanels.PanelBottom;
import gui.bottompanels.PanelLeft;
import gui.bottompanels.PanelRight;
import gui.bottompanels.PanelTop;

public class InteractiveBoard extends JPanel {
	public InteractiveBoard(code.Board boardPass){
		this.setLayout(new BorderLayout());
		this.add(new PanelTop(boardPass), BorderLayout.PAGE_START);
		this.add(new PanelBottom(boardPass), BorderLayout.PAGE_END);
		this.add(new PanelLeft(boardPass), BorderLayout.LINE_START);
		this.add(new PanelRight(boardPass), BorderLayout.LINE_END);
		this.add(new GameBoardPanel(boardPass), BorderLayout.CENTER);
		this.setBackground(Color.BLACK);
	}
}
