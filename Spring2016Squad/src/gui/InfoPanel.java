package gui;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import code.Board;
import code.Driver;
import observable.Updateable;
/**
 * This is the InfoPanel.
 * It will observer the Board, then update and display the score accordingly.
 * @author Hanming Liu
 *
 */
public class InfoPanel extends JPanel implements Updateable {
	private Board _board;
	public InfoPanel(Board b){
		_board = b;
		_board.addObs(this);
		update();
	}

	@Override
	public void update() {
		if(Driver.NUMBEROFPLAYERS==2){
			this.setLayout(new GridLayout(2,2));
			JLabel P1 = new JLabel("Player1");
			JLabel P2 = new JLabel("Player2");
			JLabel S1 = new JLabel(Driver._player1.getScore()+"");
			JLabel S2 = new JLabel(Driver._player2.getScore()+"");
			this.add(P1);
			this.add(P2);
			this.add(S1);
			this.add(S2);
			this.setVisible(true);
		}
		if(Driver.NUMBEROFPLAYERS==3){
			this.setLayout(new GridLayout(3,3));
			JLabel P1 = new JLabel("Player1");
			JLabel P2 = new JLabel("Player2");
			JLabel P3 = new JLabel("Player3");
			JLabel S1 = new JLabel(Driver._player1.getScore()+"");
			JLabel S2 = new JLabel(Driver._player2.getScore()+"");
			JLabel S3 = new JLabel(Driver._player3.getScore()+"");
			this.add(P1);
			this.add(P2);
			this.add(P3);
			this.add(S1);
			this.add(S2);
			this.add(S3);
			this.setVisible(true);
		}
		if(Driver.NUMBEROFPLAYERS==4){

			this.setLayout(new GridLayout(2,4));
			JLabel P1 = new JLabel("Player1");
			JLabel P2 = new JLabel("Player2");
			JLabel P3 = new JLabel("Player3");
			JLabel P4 = new JLabel("Player4");
			JLabel S1 = new JLabel(Driver._player1.getScore()+"");
			JLabel S2 = new JLabel(Driver._player2.getScore()+"");
			JLabel S3 = new JLabel(Driver._player3.getScore()+"");
			JLabel S4 = new JLabel(Driver._player4.getScore()+"");
			P1.setPreferredSize(new Dimension(200,200));
			P2.setPreferredSize(new Dimension(200,200));
			P3.setPreferredSize(new Dimension(200,200));
			P4.setPreferredSize(new Dimension(200,200));
			S1.setPreferredSize(new Dimension(200,200));
			S2.setPreferredSize(new Dimension(200,200));
			S3.setPreferredSize(new Dimension(200,200));
			S4.setPreferredSize(new Dimension(200,200));
			this.add(P1);
			this.add(P2);
			this.add(P3);
			this.add(P4);
			this.add(S1);
			this.add(S2);
			this.add(S3);
			this.add(S4);
			this.setVisible(true);
		}
	}


}

