package test;

import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

import code.Board;
import code.tile.Tile;

/**
 * push method tests. push return type being correct.
 * 
 * @author Matthew
 */
public class TestsForPush extends Testing{
	private Board _board;
	
	@Before public void pretest(){
		_board = new Board();
	}

	@Test public void pushtest01(){

		boolean expected = false;
		boolean actual = _board.push('n',0);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest02(){

		boolean expected = false;
		boolean actual = _board.push('e',0);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest03(){

		boolean expected = false;
		boolean actual = _board.push('s',0);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest04(){

		boolean expected = false;
		boolean actual = _board.push('w',0);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest05(){

		boolean expected = true;
		boolean actual = _board.push('n',1);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest06(){

		boolean expected = true;
		boolean actual = _board.push('s',1);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest07(){

		boolean expected = true;
		boolean actual = _board.push('e',1);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest08(){

		boolean expected = true;
		boolean actual = _board.push('w',1);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest09(){

		boolean expected = false;
		boolean actual = _board.push('n',2);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest10(){

		boolean expected = false;
		boolean actual = _board.push('e',2);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest11(){

		boolean expected = false;
		boolean actual = _board.push('s',2);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest12(){

		boolean expected = false;
		boolean actual = _board.push('w',2);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest13(){

		boolean expected = true;
		boolean actual = _board.push('n',3);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest14(){

		boolean expected = true;
		boolean actual = _board.push('e',3);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest15(){

		boolean expected = true;
		boolean actual = _board.push('s',3);
		assertTrue("",expected == actual);
	}

	@Test public void pushtest16(){

		boolean expected = true;
		boolean actual = _board.push('w',3);
		assertTrue("",expected == actual);
	}
	
	@Test public void availableTileMovementsTest(){
		
		String s = "" + _board.availableTileMovements(_board.getBoardTile(3, 3)).size();
		System.out.println(s);
		assertTrue(true);
	}

	@Test public void availableTileMovementsTest1(){
		
		String s = "" + _board.availableTileMovements(_board.getBoardTile(3, 3)).size();
		System.out.println(s);
		assertTrue(true);
	}
	
	@Test public void availableTileMovementsTest2(){
		
		String s = "" + _board.availableTileMovements(_board.getBoardTile(3, 3)).size();
		System.out.println(s);
		assertTrue(true);
	}
	
	@Test public void availableTileMovementsTest3(){
		
		String s = "" + _board.availableTileMovements(_board.getBoardTile(3, 3)).size();
		System.out.println(s);
		assertTrue(true);
	}
	@Test public void BoardsizeIsExaclyRight_x(){
		boolean number1 = true;
		boolean number2 = false;
		_board.getBoardTile(code.Board.BOARDSIZE-1,code.Board.BOARDSIZE-1);
		try{
			_board.getBoardTile(code.Board.BOARDSIZE,0);
		}
		catch(Exception e){
			number2 = true;
		}
		assertTrue(number1&&number2);
	}
	@Test public void BoardsizeIsExaclyRight_y(){
		boolean number1 = true;
		boolean number2 = false;
		_board.getBoardTile(code.Board.BOARDSIZE-1,code.Board.BOARDSIZE-1);
		try{
			_board.getBoardTile(0,code.Board.BOARDSIZE);
		}
		catch(Exception e){
			number2 = true;
		}
		assertTrue(number1&&number2);
	}
	private void comonRandomLocationofBoardTiletest(){
		int x = ThreadLocalRandom.current().nextInt(0,code.Board.BOARDSIZE);
		int y = ThreadLocalRandom.current().nextInt(0,code.Board.BOARDSIZE);
		System.out.println("RandomLocationofBoardTiletest x="+x+" y="+y);
		Tile t = _board.getBoardTile(x,y);
		Point p1 = _board.LocationofBoardTile(t);
		assertTrue(p1.x==x&&p1.y==y);
		
	}

	@Test public void LocationofBoardTileTest00(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest01(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest02(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest03(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest04(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest05(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest06(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest07(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest08(){comonRandomLocationofBoardTiletest();}
	@Test public void LocationofBoardTileTest09(){comonRandomLocationofBoardTiletest();}
}


