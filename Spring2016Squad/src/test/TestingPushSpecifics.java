package test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import code.Board;
import code.tile.Tile;

public class TestingPushSpecifics extends Testing{
	
	private Board _board;
	private Tile _center;
	private Tile _n;
	private Tile _ne;
	private Tile __e;
	private Tile _se;
	private Tile _s;
	private Tile _sw;
	private Tile __w;
	private Tile _nw;
	
	@Before public void pretest(){
		_board = new Board();
		_center = _board.getBoardTile(3,3);	//     center
		_n = _board.getBoardTile(3,2);		//true north
		_ne = _board.getBoardTile(4,2);		//     northeast
		__e = _board.getBoardTile(4,3);		
		_se = _board.getBoardTile(4,4);		
		_s = _board.getBoardTile(3,4);		
		_sw = _board.getBoardTile(2,4);		
		__w = _board.getBoardTile(2,3);		
		_nw = _board.getBoardTile(2,2);		
	}
	
	public boolean comonCheckCode(){
		// all diagonal tiles references are same?
		return 
				(_ne == _board.getBoardTile(4,2)) &&
				(_se == _board.getBoardTile(4,4)) &&
				(_sw == _board.getBoardTile(2,4)) &&
				(_nw == _board.getBoardTile(2,2));
	}
	@Test public void test00(){
		// push direction north
		_board.push('n', 3);
		assertTrue("", comonCheckCode() &&
				(_center == _board.getBoardTile(3,2)) &&
				(_s == _board.getBoardTile(3,3)) &&
				(__w == _board.getBoardTile(2,3)) &&
				(__e == _board.getBoardTile(4,3)) &&
				(_n == _board.getBoardTile(3,1))
				
				
				);
	}
	@Test public void test01(){
		// push direction west
		_board.push('w', 3);
		assertTrue("", comonCheckCode() &&
				(_center == _board.getBoardTile(2,3)) &&
				(_s == _board.getBoardTile(3,4)) &&
				(__w == _board.getBoardTile(1,3)) &&
				(__e == _board.getBoardTile(3,3)) &&
				(_n == _board.getBoardTile(3,2))
				
				
				);
	}
	@Test public void test02(){
		// push direction south
		_board.push('s', 3);
		assertTrue("", comonCheckCode() &&
				(_center == _board.getBoardTile(3,4)) &&
				(_s == _board.getBoardTile(3,5)) &&
				(__w == _board.getBoardTile(2,3)) &&
				(__e == _board.getBoardTile(4,3)) &&
				(_n == _board.getBoardTile(3,3))
				
				
				);
	}
	@Test public void test03(){
		// push direction east
		_board.push('e', 3);
		assertTrue("", comonCheckCode() &&
				(_center == _board.getBoardTile(4,3)) &&
				(_s == _board.getBoardTile(3,4)) &&
				(__w == _board.getBoardTile(3,3)) &&
				(__e == _board.getBoardTile(5,3)) &&
				(_n == _board.getBoardTile(3,2))
				
				
				);
	}
	
	
	
	
}
