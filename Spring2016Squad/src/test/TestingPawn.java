package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import code.Board;
import code.Pawn;
import code.tile.Tile;

public class TestingPawn extends Testing{

	/**
	 * @author Sangwoo
	 * Testing Pawn Cases:
	 * 1. Initializing a board.
	 * 2. Testing the destination of Pawn as address of BoardTile
	 * (This task is for Board class)3. Testing the opened directions & paths. If return same value, test successful.
	 * (PushedTile method was deleted)4. Testing the destination of Pawn when the BoardTile was pushed 
	 * 		2 cases:
	 * 			1. When Pawn was not standing on the edge of the board.
	 * 			2. When Pawn was standing on the edge of the board.)
	 * 
	 */

	private Pawn pawn;
	private Board _board = new Board();
	private Tile _boardTile = new Tile();

	@Before public void initialize(){

		pawn = new Pawn( _board/*new Board()*/, _boardTile);
		//_board = new Board();

	}

	//	private Board _board;
	//	public TestingPawn(Board bard){
	//		_board = bard;
	//	}


	}
