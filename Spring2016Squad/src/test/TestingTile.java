package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import code.tile.Tile;

public class TestingTile extends Testing{

	private Tile tile;


	/**
	 * Creates a new instance of the BoardTile class to run the tests on.
	 * 
	 */
	@Before public void initialise(){
		tile = new Tile();
	}

	//	/**
	//	 * Random test case (can be true/false)
	//	 */
	//	@Test public void test01(){
	//		assertTrue(tile.get_north()==true);
	//	}

	/**
	 * Test to verify that BoardTile class produces variations of all the tiles the game uses.
	 * 
	 * if number == 2, tile is
	 * 			_________	
	 * 			|  |  |  |	
	 * 			|  |  |	 |	
	 * 			|__|__|__|	
	 * 			OR
	 * 			________
	 * 			|__| |	|
	 * 			|____|	|
	 * 			|_______|
	 * 
	 * if number == 3, tile can be
	 * 	       ________
	 * 	   	   |  | |__|      
	 * 	       |  |  __|
	 * 		   |__|_|__|
	 * 
	 * @author (javadoc) Aniruddha
	 */
	@Test public void test02(){
		int number = 0;
		if (tile.get_north()){
			number++;
		}
		if (tile.get_east()){
			number++;
		}
		if (tile.get_south()){
			number++;
		}
		if (tile.get_west()){
			number++;
		}

		assertTrue(number >= 2 && number <=3);
	}
	@Test public void test03(){
		tile = new Tile(false, true, true, true);
		tile.rotateCounterClockwise();
		assertTrue(tile.equals(new Tile(true,true,true,false)));
	}
	@Test public void test04(){
		tile = new Tile(false, true, false, true);
		tile.rotateCounterClockwise();
		assertTrue(tile.equals(new Tile(true,false,true,false)));
	}
	private void randomtestClockwise(){
		Tile test = new Tile(tile);
		test.rotateClockwise();
		assertTrue((tile.get_north()==test.get_east())&&(tile.get_east()==test.get_south())&&(tile.get_west()==test.get_north())&&(tile.get_south()==test.get_west()));
	}
	@Test public void testClockwise00(){randomtestClockwise();}
	@Test public void testClockwise01(){randomtestClockwise();}
	@Test public void testClockwise02(){randomtestClockwise();}
	@Test public void testClockwise03(){randomtestClockwise();}
	@Test public void testClockwise04(){randomtestClockwise();}
	@Test public void testClockwise05(){randomtestClockwise();}
	@Test public void testClockwise06(){randomtestClockwise();}
	@Test public void testClockwise07(){randomtestClockwise();}
	
	@Test public void testIngridiantAdd(){
		tile.takeIngrediant(3);
		int expected = tile.giveIngrediant();
		assertTrue(expected == 3);
	}
	@Test public void testCannotHaveTwoIngr(){
		boolean truth = false;
		tile.takeIngrediant(3);
		try{
			tile.takeIngrediant(3);
		}
		catch(IllegalStateException e){
			truth = true;
		}
		assertTrue(truth);
	}
	@Test public void testIngrediantTaken(){
		
	}
	@Test public void testPeek(){
		assertTrue("String is:"+tile.peek(),tile.peek().equals(""));
	}
	@Test public void testPeekFull(){
		tile.takeIngrediant(5);
		assertTrue("String is:"+tile.peek(),"5".equals(tile.peek()));
	}
	







}
