package observable;

public interface Updateable {
	public void update(); 
}
